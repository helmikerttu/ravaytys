#ifndef PLAYER_H
#define PLAYER_H
#include <string>


class Player {

public:
    Player(int id, std::string name);
    ~Player();

    // Returns the id of the player
    int getId() const;
    // Returns the name of the player
    std::string getName() const;
    // Returns the amount of points the player has
    int getPoints() const;
    // Adds points to the player
    void addPoints(int pointsToAdd);

private:
    int _playerId;
    std::string _playerName;
    int _points;

};

#endif // PLAYER_H
