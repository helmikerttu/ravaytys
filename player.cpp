#include "player.h"


Player::Player(int id, std::string name) :
    _playerId(id),
    _playerName(name),
    _points(0)
{}

Player::~Player(){}


int Player::getId() const
{
    return _playerId;
}

std::string Player::getName() const
{
    return _playerName;
}

int Player::getPoints() const
{
    return _points;
}

void Player::addPoints(int pointsToAdd)
{
    _points = _points + pointsToAdd;
}
