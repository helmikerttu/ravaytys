#include "gamewindow.h"
#include "ui_gamewindow.h"

#include <QInputDialog>
#include <QMessageBox>


GameWindow::GameWindow(std::vector<QString> _playersVec, QWidget *parent) :
    QWidget(parent),
    _currentRound(0),
    ui(new Ui::GameWindow)
{
    ui->setupUi(this);

    connect(ui->nextRoundButton, &QPushButton::clicked, this, &GameWindow::nextRound);
    connect(ui->addPontsButton, &QPushButton::clicked, this, &GameWindow::addPoints);
    connect(ui->quitButton, &QPushButton::clicked, this, &GameWindow::close);

    initPlayers(_playersVec);

    ui->ruleTable->setRowCount(7);
    ui->ruleTable->setColumnCount(3);

    ui->playerTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->ruleTable->setEditTriggers(QAbstractItemView::NoEditTriggers);

    nextRound();
}


GameWindow::~GameWindow()
{
    delete ui;
}


void GameWindow::initPlayers(std::vector<QString> _playersVec)
{
    ui->playerTable->setRowCount(static_cast<int>(_playersVec.size()));
    ui->playerTable->setColumnCount(2);
    for (unsigned int i = 0; i < _playersVec.size(); ++i){
        int j = static_cast<int>(i);
        QTableWidgetItem* name_item = new QTableWidgetItem;

        name_item->setText(_playersVec.at(i));
        ui->playerTable->setItem(j, 0, name_item);

        QTableWidgetItem* points_item = new QTableWidgetItem;

        QString points = QString::number(0);
        points_item->setText(points);
        ui->playerTable->setItem(j, 1, points_item);

        std::string name = _playersVec.at(i).toStdString();
        std::shared_ptr<Player> player = std::make_shared<Player>(i, name);
        _players.insert({i, player});
    }
}


void GameWindow::addPoints()
{
    if (ui->playerTable->currentItem() != nullptr){
        int points = QInputDialog::getInt(this, "Add points", "The amount of points to add");
        int row = ui->playerTable->currentRow();
        _players.at(row)->addPoints(points);

        QTableWidgetItem* points_item = ui->playerTable->item(row, 1);
        points_item->setText(QString::number(_players.at(row)->getPoints()));

    }
}


void GameWindow::nextRound()
{
    switch (_currentRound) {
        case 0:
            _currentRound = 1;
            setGoal(0, "10", "2", "0");
            break;
        case 1:
            _currentRound = 2;
            setGoal(1, "10", "1", "1");
            break;
        case 2:
            _currentRound = 3;
            setGoal(2, "10", "0", "2");
            break;
        case 3:
            _currentRound = 4;
            setGoal(3, "12", "3", "0");
            break;
        case 4:
            _currentRound = 5;
            setGoal(4, "12", "2", "1");
            break;
        case 5:
            _currentRound = 6;
            setGoal(5, "12", "1", "2");
            break;
        case 6:
            _currentRound = 7;
            setGoal(6, "12", "0", "3");
            ui->nextRoundButton->setText("End game");
            break;
        case 7:
            endGame();
            break;
        default:
            _currentRound = 0;
    };
}

void GameWindow::setGoal(int row, QString cards_text, QString kinds_text, QString straights_text)
{
    QTableWidgetItem* cards_item = new QTableWidgetItem();
    cards_item->setText(cards_text);
    ui->ruleTable->setItem(row, 0, cards_item);

    QTableWidgetItem* kinds_item = new QTableWidgetItem();
    kinds_item->setText(kinds_text);
    ui->ruleTable->setItem(row, 1, kinds_item);

    QTableWidgetItem* straights_item = new QTableWidgetItem();
    straights_item->setText(straights_text);
    ui->ruleTable->setItem(row, 2, straights_item);
}


void GameWindow::endGame()
{
    std::vector<std::pair<int, QString>> losers {{-1, ""}};
    for (int i = 0; i < static_cast<int>(_players.size()); ++i){
        int points = _players.at(i)->getPoints();
        if (points >= losers.at(0).first){
            if (points > losers.at(0).first) {
                losers.clear();
            }
            QString name = QString::fromStdString(_players.at(i)->getName());
            std::pair<int, QString> newloser = {points, name};
            losers.push_back(newloser);
        }
    }
    QString losertext = losers.at(losers.size() - 1).second;
    losers.pop_back();
    for (std::pair<int, QString> loser : losers){
        losertext = losertext + " and " + loser.second;
    }
    losertext = losertext + ", you suck the most!";
    QMessageBox* messageBox = new QMessageBox;
    messageBox->setWindowTitle("Game over!");
    messageBox->setText(losertext);
    messageBox->exec();

    ui->nextRoundButton->hide();
    ui->addPontsButton->hide();
}
