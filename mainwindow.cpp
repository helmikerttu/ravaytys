#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "gamewindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->startButton, &QPushButton::clicked, this, &MainWindow::startGame);
    connect(ui->closeButton, &QPushButton::clicked, this, &MainWindow::close);

    connect(ui->addButton, &QPushButton::clicked, this, &MainWindow::addPlayerToList);
    connect(ui->removeButton, &QPushButton::clicked, this, &MainWindow::removePlayerFromList);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startGame()
{
    QList<QListWidgetItem *> items =
          ui->listWidget->findItems(QString("*"), Qt::MatchWrap | Qt::MatchWildcard);

    for (QListWidgetItem* item : items){
        _playersVec.push_back(item->text());
    }
    if (_playersVec.size() > 0){
        GameWindow* game = new GameWindow(_playersVec);
        game->show();
        close();
    }
}

void MainWindow::addPlayerToList()
{
    // Max number of players reached
    if (_playersVec.size() == 6){
        return;
    }
    // No players with the same name allowed
    QString name = ui->nameEdit->text();
    for (QString player : _playersVec){
        if (name == player)
        {
            return;
        }
    }
    ui->listWidget->addItem(name);
    ui->nameEdit->clear();
}

void MainWindow::removePlayerFromList()
{
    if (ui->listWidget->currentItem() != nullptr) {
        ui->listWidget->takeItem(ui->listWidget->currentRow());
    }

}
