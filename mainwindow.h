#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QListWidget>
#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:

    // Starts the game
    void startGame();
    // Adds players name to the list
    void addPlayerToList();
    // Removes players name from the list
    void removePlayerFromList();

private:
    std::vector<QString> _playersVec;
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
