#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTableWidget>
#include <QWidget>
#include <vector>
#include <unordered_map>
#include <memory>

#include "player.h"

namespace Ui {
class GameWindow;
}

class GameWindow : public QWidget
{
    Q_OBJECT


public:

    explicit GameWindow(std::vector<QString> _playersVec, QWidget *parent = nullptr);
    ~GameWindow();

    void initPlayers(std::vector<QString> _playersVec);

public slots:

    // Adds points to the player
    void addPoints();
    // Changes the round
    void nextRound();
    // Adds the goal of the current round to the table
    void setGoal(int row, QString cards_text, QString kinds_text, QString straights_text);
    // Ends the game
    void endGame();

private:
    int _currentRound;
    std::unordered_map<int, std::shared_ptr<Player>> _players;
    Ui::GameWindow *ui;
};

#endif // GAMEWINDOW_H
