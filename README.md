# Räväytys

Räväytys is an awesome card game also known as Sanghai. Its rules vary slightly depending on who's playing, but this is how my grandma once taught me.

## General

The game is played with 2-6 players and two decks. It consist of seven separate rounds, each with its own goal. Players, other than the round winner, get points after each round. The player with the most points after the last round, loses the game. This application is a supporting tool for the game. It keeps track of the current round and its goals revealing the loser/losers of the game at the end. It also allows you to record and track everyone's points.

## Goal

The main goal is to get as few points as possible. To do that, you must try to get rid of your cards each round. You can get rid of the cards when you reach that round's goal. Each round the goal is to collect three of a kinds and straight flushes. Both of them require at least three cards, but nothing prevents you from collecting more. That way you can get rid of more cards at a time.

1. Round: you must collect two three of a kinds, for example 777 and QQQ
2. Round: collect three of a kind and one straight flush
3. Round: two straight flushes
4. Round: three three of a kinds
5. Round: two three of a kinds and one straight flush
6. Round: three of a kind and two straight flushes
7. Round: three straight flushes


## Cards

The amount of cards dealt depends on the round. Rounds 1 to 3 each player are dealt 10 cards and rounds 4 to 7, 12 cards. The dealer gets one extra card.

The game is played with two decks and jokers are included. Jokers can represent any card and aces can be 1 or 14, depending on the player's choice

## Points

In this game, points are a bad thing to have. You get points from the cards that are left in your hand after each round. Some cards give you more points than the others.

- 2-10: 5 points
- JQK: 10 points
- A: 15 points
- Joker: 50 points

## Gameplay

The dealer starts by playing one card face up on the table. 

The next player can draw the card from the table or draw a new card from the deck. The player ends his turn by playing one card face up on the table. The cards on the table are stacked, so that only the top card can be drawn by the next player.

Once you get the cards needed for the round, you can place them on the table in piles for everyone to see. This is called 'opening your game' and you can only do it on your own turn.

After that, the game goes on just like it did before and you can keep collecting those cards and adding them to the piles on your own turn. You can also add cards to other peoples piles after your have opened your game.

If you have jokers in your piles, other players, who haven't opened their game yet, may replace them with the cards the jokers represent, on their own turn.

The player who first gets rid of their cards, wins the round. Other players get points from the cards left in their hand (the cards on the table won't count). 

If you open your game and win all at once, you do the Räväytys.

